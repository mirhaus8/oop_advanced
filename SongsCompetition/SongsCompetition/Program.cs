﻿using System;

namespace SongsCompetition
{
    class Program
    {
        static void Main(string[] args)
        {
            Competitor comp1 = new Competitor("gaga", "gaga Song");
            Competitor comp2 = new Competitor("cristina", "cristina Song");
            Referee r = new Referee("simon");
            Competition competition = new Competition();
            competition.CompetitionRound(r, comp1, comp2);
        }
    }
}
