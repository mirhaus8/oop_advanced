﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SongsCompetition
{
    class Competitor:Person
    {
        public string Song { get; set; }
        public Competitor(string name, string song) : base(name) { this.Song = song; }

        public void Sing()
        {
            Console.WriteLine($"{this.Name} singing - {this.Song}");
        }


    }
}
