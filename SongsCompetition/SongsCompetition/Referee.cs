﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SongsCompetition
{
    class Referee:Person
    {
        public Referee(string name) : base(name) { }

        public int ChooseTheWinner()
        {
            Random rand = new Random();
            return rand.Next(2);
        }
    }
}
