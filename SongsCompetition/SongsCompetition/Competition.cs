﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SongsCompetition
{
    class Competition
    {
        public void CompetitionRound(Referee referee, Competitor firstCompetitor, Competitor secondCompetitor)
        {
            firstCompetitor.Sing();
            secondCompetitor.Sing();
            int winner = referee.ChooseTheWinner();
            switch (winner)
            {
                case 0:
                    Console.WriteLine($"the first singer( - {firstCompetitor.Name} ) won");
                    break;
                case 1:
                    Console.WriteLine($"the second singer( - {secondCompetitor.Name} ) won");
                    break;
                default:
                    break;
            }
        }
    }
}
